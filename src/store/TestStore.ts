import { writable } from "svelte/store";
import type { Character, Power, Perk, Flaw } from "../lib/types/character";
import { data } from "../lib/data/pnpdata";

export const newCharacter: Character = {
    heroName: "",
    realName: "",
    edge: 0,
    health: 0,
    resolve: 0,
    heroPoints: 0,
    abilities: data.abilities,
    talents: data.talents,
    powers: [],
    perks: [],
    flaws: [],
    gear: []
}

export const powers: Power[] = [
    {
        id: 0,
        name: "",
        rank: 1,
        description: "",
        cost: 1,
        source: "",
        prosAndCons: []
    }
];

// export const pros: Pro[] =[
//     {
//         id: 0,
//         name: "",
//         cost: 1,
//         description:""    
//     }
// ];

// export const cons: Con[] =[
//     {
//         id: 0,
//         name: "",
//         refund: 1,
//         description:""    
//     }
// ];

export const perks: Perk[] =[
    {
        id: 0,
        name: "",
        cost: 1,
        description:""    
    }
];

export const flaw: Flaw[] =[
    {
        id: 0,
        name: "",
        description:""    
    }
];