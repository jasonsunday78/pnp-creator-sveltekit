export const data = {
	abilities: [
		{
			id: `abil1`,
			name: `agility`,
			rank: 1,
			description: `Agility covers coordination, dexterity, nimbleness, reflexes, and speed. It is used to perform acrobatic or athletic activities like balancing, climbing, diving, jumping, and running. Agility also applies when firing mundane ranged weapons and defending against attacks. Last, you can substitute half your Agility for your Covert when making challenge rolls to hide, move quietly, or avoid detection.`,
			cost: 1
		},
		{
			id: `abil2`,
			name: `intellect`,
			rank: 1,
			description: `Intellect represents your intelligence, knowledge, memory, shrewdness, and overall level of education. Use this Trait to make intelligence-based challenge rolls that don’t fall under any of your Talents. In fact, you can substitute half your Intellect for any Talent when making challenge rolls to determine what you know about something (but not when trying to actually do anything — knowing how to perform a heart transplant, for example, is very different from actually performing one)`,
			cost: 1
		},
		{
			id: `abil3`,
			name: `might`,
			rank: 1,
			description: `Might is your sheer physical strength. It is used to perform armed and unarmed close combat attacks and feats of strength like lifting heavy objects or tearing things apart. The Weight table indicates the maximum amount of weight you can lift, depending on your Might. When not referring to Might, the rank that corresponds to an object’s weight on this table is sometimes called its weight rank. Last, Might can also come into play when determining your Health.`,
			cost: 1
		},
		{
			id: `abil4`,
			name: `perception`,
			rank: 1,
			description: `Perception covers your sensory acuity, empathy, intuition, and overall level of awareness. It’s used any time you need to detect or discern hidden things, from assailants to clues to intentions. It also indicates how quickly you can detect, process, and react to new information.`,
			cost: 1
		},
		{
			id: `abil5`,
			name: `toughness`,
			rank: 1,
			description: `Toughness combines your constitution, endurance, and physical resilience. It’s used to resist Powers that affect you physically, as well as physical agents or toxins like diseases, drugs, and poisons. Last, Toughness helps determine how much Health you have and how quickly you recover from injuries.`,
			cost: 1
		},
		{
			id: `abil6`,
			name: `willpower`,
			rank: 1,
			description: `Willpower represents your courage, determination, and self- discipline. It also indicates the power of your spirit or psyche. It’s used to defend against Powers that affect the mind or soul, and to resist negative emotions and impulses. It can also come into play when determining your Health.`,
			cost: 1
		}
	],
	talents: [
		{
			id: `talent1`,
			name: `academics`,
			rank: 1,
			description: `Academics represents knowledge of scholarly subjects like anthropology, archaeology, art, cultures, history, linguistics, philosophy, psychology, and sociology. It also covers esoteric and fantastical topics like dimensional geography, interplanetary news, magic, the occult, mythical beings, creatures, and places, and so on. If you have at least 4d in this Talent, you are highly educated or well-read.`,
			cost: 1
		},
		{
			id: `talent2`,
			name: `charm`,
			rank: 1,
			description: `Charm reflects attractiveness, empathy, social skills, and force of personality. You use this Talent whenever you try to bluff, charm, deceive, negotiate with, persuade, or seduce someone. Acting also falls under Charm, even when you aren’t using it to deceive anyone.`,
			cost: 1
		},
		{
			id: `talent3`,
			name: `command`,
			rank: 1,
			description: `Command concerns getting others to do what you want through force of will. Use this Talent whenever you try to command, frighten, intimidate, interrogate, motivate, or order people around. Apart from that, Command also covers establishing and understanding command structures and protocols.`,
			cost: 1
		},
		{
			id: `talent4`,
			name: `covert`,
			rank: 1,
			description: `Covert applies to intrusion, larceny, shadowing, stealth, and surveillance. It’s used whenever you need to hide, move quietly, follow someone, conduct covert surveillance, or otherwise avoid detection. Covert also helps defeat security measures like alarms, cameras, locks, and traps, as well as to palm items, pick pockets, and perform other acts of nimble-fingered larceny.`,
			cost: 1
		},
		{
			id: `talent5`,
			name: `investigation`,
			rank: 1,
			description: `Investigation concerns the ability to conduct investigations, uncover clues, perform research, interrogate witnesses, analyze data, and make deductive or intuitive leaps. If you have at least 4d in this Talent, you are well-versed in police procedures and your investigations may be admissible in court.`,
			cost: 1
		},
		{
			id: `talent6`,
			name: `medicine`,
			rank: 1,
			description: `Medicine covers academic and clinical knowledge of medicine and psychology. Assuming you have medical supplies on hand, you can treat wounded characters by making a Hard (2) Medicine roll, healing them of 1 point of damage per net success rolled. Raise the difficulty to Daunting (3) when treating yourself. You can only do this once per patient per day (or once per patient per week if using the Gritty Combat Rules in Chapter 4). If you have at least 4d in this Talent, you are probably a trained doctor. Your parents must be very proud.`,
			cost: 1
		},
		{
			id: `talent7`,
			name: `professional`,
			rank: 1,
			description: `Professional reflects your knowledge of the professional world and covers topics like business, economics, and finance. While you may also have a basic understanding of more specific professional areas such as accounting, journalism, law, marketing, politics, or public relations, the Expertise Power represents in-depth knowledge of a specific professional career.`,
			cost: 1
		},
		{
			id: `talent8`,
			name: `science`,
			rank: 1,
			description: `Science covers your knowledge of scientific topics including disciplines like astronomy, biology, botany, chemistry, ecology, genetics, geology, metallurgy, oceanography, physics, and quantum mechanics. It also covers esoteric or fantastical science like paranormal biology or the unique laws of physics in Dimension X! If you have at least 4d in this Talent, you are probably a trained scientist.`,
			cost: 1
		},
		{
			id: `talent9`,
			name: `streetwise`,
			rank: 1,
			description: `Streetwise represents your knowledge of important locations, personalities, and happenings in the criminal underworld, as well as your ability to interact with them. If you have at least 4d in this Talent, your parents probably aren’t nearly as proud as they would have been if you’d gone to med school.`,
			cost: 1
		},
		{
			id: `talent10`,
			name: `survival`,
			rank: 1,
			description: `Survival concerns your ability to survive in the wild. It’s used when building fires, finding food and water, securing shelter, setting traps and snares, tracking, and so on. It also covers practical information about plants and animals found in the wild. Last, this Talent applies when handling animals or riding mounts. If you have at least 4d in this Talent, you may also know how to care for, tame, and train animals.`,
			cost: 1
		},
		{
			id: `talent11`,
			name: `technology`,
			rank: 1,
			description: `Technology deals with the creation, modification, and repair of mechanical and technological devices of every kind. This includes computers, electronics, machines, robots, steampunk technology, vehicles, weapons, and anything else you can think of. It also covers the use (and misuse) of high-tech systems like computers and computer networks, as well as equipment like communications and sensor systems. Assuming you have the right tools, you can repair damaged objects by making a Hard (2) Technology roll, repairing 1 point of damage per net success rolled. You can only do this once per object per day.`,
			cost: 1
		},
		{
			id: `talent12`,
			name: `vehicles`,
			rank: 1,
			description: `Vehicles covers the ability to operate all sorts of vehicles. It applies not only to piloting, but also to gunnery, navigation, operations, and even basic maintenance. As mentioned earlier, it might be reasonable to assume you can only drive ordinary vehicles like cars and motorcycles unless you have at least 4d in this Talent, but the GM always has the final say on this.`,
			cost: 1
		}
	],
	perks: [
		{
			id: `perk1`,
			name: `authority`,
			cost: 3,
			description: `You have more authority, rights, or social power than the average person. This could include things like access to secret government installations, diplomatic immunity, top secret security clearance, authority to arrest and detain people, or just being a member of the nobility or ruling class.`
		},
		{
			id: `perk2`,
			name: `contacts`,
			cost: 1,
			description: `You know people who can provide you with information and the occasional favor. Common examples include academic, business, criminal, legal, media, military, police, political, and scientific contacts.`
		},
		{
			id: `perk3`,
			name: `fame`,
			cost: 3,
			description: `You are famous. People who recognize you generally react favorably, and some will fall all over themselves trying to get your attention, an autograph, or a selfie with you. If you have a secret identity, you must decide whether this Perk applies to your civilian identity or your heroic alter ego.`
		},
		{
			id: `perk4`,
			name: `fourth wall`,
			cost: 6,
			description: `You believe you are a character in a comic book (and you might be right). You often break the fourth wall to interact directly with the audience. In game terms, any time you break the fourth wall and crack a joke that gets everyone at the table laughing, you earn 1 point of Resolve. But you can only do this once per scene. This Perk is best reserved for comedic or lighthearted games.`
		},
		{
			id: `perk5`,
			name: `headquarters`,
			cost: 1,
			description: `You have a base of operations or headquarters of some kind. Every Hero Point you spend on this Perk gives you 3 Base Points to spend on your headquarters. Multiple Heroes can apply their Base Points to the same headquarters. You can find rules for creating headquarters in Chapter 6.`
		},
		{
			id: `perk6`,
			name: `infamy`,
			cost: 3,
			description: `You are infamous among the criminal underworld, known as a dangerous customer and someone to be feared. Ordinary criminals are afraid of you and may fall all over themselves trying to appease you, especially when you apply the right kind of pressure. Supervillains are usually less impressed.`
		},
		{
			id: `perk7`,
			name: `patron`,
			cost: 3,
			description: `You are connected to a person, group, or organization that provides you with information, equipment, funding, and support for your heroic activities. Common patrons include government organizations, rebel groups, and wealthy humanitarians who just love adopting teenagers with superpowers.`
		},
		{
			id: `perk8`,
			name: `pet/sidekick`,
			cost: 1,
			description: `You have a pet or a sidekick. Every Hero Point you spend on this Perk gives you 10 Hero Points to create this character. Your companion can’t have more than half your Hero Points, and they must abide by the game’s Trait Cap. Your companion earns 1 Hero Point for every 2 that you earn. Pets and sidekicks are considered Heroes. They lack Resolve, but you can spend your Resolve on their behalf. If your companion has higher Trait ranks than you, use their Traits when determining your Resolve (see Chapter 5). If your companion has Flaws, you earn Resolve as if that character were a Hero. GMs who don’t want players running two (or more) characters should feel free to limit or prohibit this Perk.`
		},
		{
			id: `perk9`,
			name: `reputation`,
			cost: 3,
			description: `You have a reputation that works to your benefit. Unlike fame, which makes people like you, this Perk means you have authority, credibility, or respect in a particular arena. For example, in your civilian life, you might be an attorney known to be an absolute terror in the courtroom. If you have a secret identity, you must decide whether this Perk applies to your civilian identity or your heroic alter ego.`
		},
		{
			id: `perk10`,
			name: `resources`,
			cost: 1,
			description: `You have access to an impressive supply of equipment of one sort or another: military weapons, ninja gear, tricked-out vehicles, etc. This often means you know someone who gets you these things or you stumbled across a hidden cache. It does not mean you are rich. For that, see Wealth.`
		},
		{
			id: `perk11`,
			name: `unique vehicle`,
			cost: 1,
			description: `You have one or more unique vehicles. Every Hero Point you spend on this Perk gives you 25 Vehicle Points to spend on your vehicles. Rules for creating unique vehicles are in Chapter 6.`
		},
		{
			id: `perk12`,
			name: `wealth/great wealth`,
			cost: 3,
			description: `Wealth costs 3 Hero Points and means you are rich. You aren’t just comfortably well-off; you are truly wealthy. You can buy almost anything you want, legal or otherwise. You probably own several homes, have multiple vehicles, and employ at least a few domestic servants. If you happen to be a tech-based Hero, you can easily fund your extracurricular activities out of your own pocket.

Great Wealth costs 6 Hero Points and means you are one of the wealthiest people in the world. At this level, money is almost meaningless. Not only can you buy whatever you want, but you can spend 1 point of Resolve once per issue to make a truly absurd purchase like buying an island or a major corporation. While many Heroes are wealthy, GMs may want to restrict Great Wealth because characters with this much money can spend their way around most problems without batting an eye.`
		}
	],
	flaws: [
		{
			id: `flaw1`,
			name: `absentminded`,
			description: `You tend to forget things. Usually, the things you forget aren’t all that important. But sometimes they are. Whenever that happens, you earn a point of Resolve.`
		},
		{
			id: `flaw2`,
			name: `alter ego`,
			description: `You have a mundane alter ego. Your alter ego is built with 50 Hero Points and earns 1 Hero Point for every 5 you earn. Decide how your transformation works and whether it has any
limitations when you select this Flaw. Any time this causes you trouble, you earn a point of Resolve.`
		},
		{
			id: `flaw3`,
			name: `amnesia`,
			description: `You have no memory of your past, although you may be haunted by fragments in dreams or visions. You don’t know who you are or where you come from. People who know this can take advantage of you, and your past will come back to haunt you every so often. This Flaw is both a Plot Hook and a Condition, so it grants you 1 extra point of Resolve at the start of every issue`
		},
		{
			id: `flaw4`,
			name: `aversion/fear`,
			description: `You have an aversion to or fear of something, and you will do whatever you can to avoid it. If you can’t, you may become hysterical, nauseated, violent, or worse. You may even pass out. However it works, you earn a point of Resolve whenever you succumb to this Flaw. Common fears include a fear of the dark, enclosed spaces, fire, heights, insects, mice, public speaking,
snakes, spiders, and water.`
		},
		{
			id: `flaw5`,
			name: `beast`,
			description: `Your mental faculties are about on par with a bright animal or a game designer. You act mostly on instinct, and complex reasoning is beyond you. You earn a point of Resolve any time you can’t figure out how to do things like open a door or get the sheriff to understand that Billy fell down a well.`
		},
		{
			id: `flaw6`,
			name: `blind/deaf`,
			description: `You are completely blind or deaf (pick one). If you are blind, you may have to select at least one Power that compensates enough to let you operate as a superhero (discuss this with your GM). This Flaw is a Condition that grants you 1 extra point of Resolve at the start of every issue.`
		},
		{
			id: `flaw7`,
			name: `broke`,
			description: `You are penniless, jobless, and possibly homeless. You can’t buy anything, so you have to beg, borrow, or steal what you need. Every time this causes you problems, you earn a point of Resolve.`
		},
		{
			id: `flaw8`,
			name: `clumsy`,
			description: `For whatever reason, you tend to break things. Your allies know to keep the fragile stuff away from you. Nevertheless, accidents happen. When they do, you earn a point of Resolve.`
		},
		{
			id: `flaw9`,
			name: `code`,
			description: `You follow a rigid code of conduct, one that requires more of you than just acting heroically. You earn a point of Resolve whenever following your code hurts you or your allies.`
		},
		{
			id: `flaw10`,
			name: `color blind`,
			description: `You see in black and white, or you have trouble distinguishing certain colors (reds and greens being the usual culprits). You earn a point of Resolve whenever this causes you problems.`
		},
		{
			id: `flaw11`,
			name: `Compulsion (Severe)`,
			description: `You have trouble resisting the urge to do something like drinking, gambling, chasing potential romantic partners, grandstanding, proving that your intellect is unmatched, or working on that stupid roleplaying game all night long. You earn a point of Resolve any time this works to your detriment. If the compulsion is literally impossible to resist, then you have a Severe Compulsion, which is a Condition that grants you 1 extra point of Resolve at the start of every issue. If you have a Severe Compulsion, you must describe the specific environment, situation, etc. that triggers the compulsion.`
		},
		{
			id: `flaw12`,
			name: `creepy`,
			description: `You give off a vibe that unnerves sensitive observers like young children, psychics, and animals. Animals usually shy away from you, unless they’re predators, in which case they attack you first. You earn a point of Resolve whenever your unsettling aura causes you trouble.`
		},
		{
			id: `flaw13`,
			name: `curse`,
			description: `You suffer from some supernatural burden. Make up the details and get your GM’s approval. You earn a point of Resolve whenever your curse causes problems for you or your allies.`
		},
		{
			id: `flaw14`,
			name: `decorum`,
			description: `You value propriety and feel compelled to follow social mores, even when others don’t. Manners maketh man, and all that. Whenever this makes life difficult, you earn a point of Resolve`
		},
		{
			id: `flaw15`,
			name: `disabled`,
			description: `You are disabled, possibly missing a limb or two, possibly wheelchair-bound, possibly something else. Describe the specifics when you select this Flaw. You may have to select at least one Power that compensates enough to let you operate as a superhero (discuss this with your GM). This Flaw is a Condition that grants you 1 extra point of Resolve at the start of every issue.`
		},
		{
			id: `flaw16`,
			name: `emotionless`,
			description: `You lack human empathy. Unable to understand emotions, you are often coldly logical in your interactions with others. As a result, people tend to react poorly to even your most carefully reasoned attempts at meaningful exchange. You earn a point of Resolve any time this causes you problems.`
		},
		{
			id: `flaw17`,
			name: `enemy`,
			description: `You have an enemy that wants you ruined, imprisoned, or dead. Make up whatever details you want and let your GM make up the rest. Your enemy doesn’t necessarily have to be a single individual or a super-powered nemesis. It could instead be a group, an organization, or even an entire nation. This Flaw is a Plot Hook that grants you 1 extra point of Resolve at the start of every issue.`
		},
		{
			id: `flaw18`,
			name: `finite power`,
			description: `One or more of your Powers requires ammo, energy, or some other depletable resource. You earn a point of Resolve whenever you run out of this resource at the wrong time.`
		},
		{
			id: `flaw19`,
			name: `night blind`,
			description: `You have a hard time seeing in dim lighting, which is almost like darkness to you. You earn a point of Resolve any time you suffer an adverse effect because of this condition.`
		},
		{
			id: `flaw20`,
			name: `nocturnal`,
			description: `You are wired to sleep during the day, or you have a form of narcolepsy. Either way, you tend to be tired and sluggish during the day, and you sometimes doze off when things are calm—shaving cream beards are all too familiar. Whenever this becomes an issue, you earn a point of Resolve.`
		},
		{
			id: `flaw21`,
			name: `notoriety`,
			description: `Whether deserved or not, you have a bad reputation. Maybe you’ve become the target of a newspaper editor who hates “costumed freaks.” Perhaps you used to be a supervillain and the public hasn’t forgiven or forgotten. Or you might be the unfortunate subject of blind prejudice. Whatever the case, most people react unfavorably to you. Some may even flee or call the police when you appear. When things like this happen, you sigh and earn a point of Resolve.`
		},
		{
			id: `flaw22`,
			name: `obligation`,
			description: `You have a duty or responsibility that requires you to do certain things and prevents you from doing others. Being a student and having a full-time job are common Obligations on present-day Earth. This Flaw is a Plot Hook that grants you 1 extra point of Resolve at the start of every issue.`
		},
		{
			id: `flaw23`,
			name: `outsider`,
			description: `You come from somewhere else, often another time, world, or dimension. Your home is quite different from present-day Earth, so there are bound to be faux pas and missteps. You earn a point of Resolve any time you have trouble dealing with novel objects or situations.`
		},
		{
			id: `flaw24`,
			name: `power limits`,
			description: `One or more of your Powers has certain limitations. Describe the specifics. In effect, this works like a Con that you control, but instead of making the Power less expensive, this Flaw lets you earn 1 point of Resolve whenever this works to your detriment or to that of your allies.`
		},
		{
			id: `flaw25`,
			name: `quirk`,
			description: `You have an annoying, obnoxious, or unfortunate personality trait that often gets the better of you at the worst of times. You earn a point of Resolve whenever that happens.`
		},
		{
			id: `flaw26`,
			name: `reaction (severe)`,
			description: `You must satisfy a requirement every so often or you will suffer an adverse effect. Maybe you have to perform a nightly ritual to maintain your powers, or you need regular doses of medicine to stay alive, or perhaps you are addicted to alcohol, drugs, or gambling. Make up the details when you select this Flaw. You earn a point of Resolve whenever you do something selfish or unheroic to satisfy the requirement, or when you suffer an adverse effect from not satisfying the requirement. If the requirement is desperate and immediate, like breathing water instead of air, then you have a Severe Requirement, which is a Condition that grants you 1 extra point of Resolve at the start of every issue.`
		},
		{
			id: `flaw27`,
			name: `restriction`,
			description: `You have fewer rights than the average person, often because you are a minor or an undocumented alien (maybe literally). This also applies if you are a second-class citizen, were deemed legally incompetent, etc. Whenever your restriction makes things difficult, you earn a point of Resolve.`
		},
		{
			id: `flaw28`,
			name: `secret`,
			description: `You have a secret that must be kept at all costs. You earn a point of Resolve whenever your efforts to protect your secret work to your detriment or to the detriment of your allies.`
		},
		{
			id: `flaw29`,
			name: `secret identity`,
			description: `This is the iconic version of the Secret Flaw in most comic book superhero settings.`
		},
		{
			id: `flaw30`,
			name: `slow`,
			description: `You move slowly. You earn a point of Resolve whenever you choose to automatically fail an Agility roll involving speed or movement and the failure has negative consequences for you or your allies. This Flaw is almost meaningless if you have a Travel Power that compensates for it.`
		},
		{
			id: `flaw31`,
			name: `unlucky/jinx`,
			description: `If it weren’t for bad luck, you wouldn’t have any luck at all. Unlucky means the GM can inflict a misfortune on you once per issue without needing to spend Adversity (for more on this, see Chapter 5). Jinx works the same way, but the misfortune affects a fellow Hero or ally in your vicinity instead of you. This Flaw is a Condition that grants you 1 extra point of Resolve at the start of every issue.`
		},
		{
			id: `flaw32`,
			name: `unusual looks`,
			description: `Your appearance is off-putting. You might look incredibly ugly, unnaturally beautiful, or strange and uncanny in some other way. Whatever the case, you earn a point of Resolve whenever your appearance becomes an issue. This Flaw is redundant if you have Frightening.`
		},
		{
			id: `flaw33`,
			name: `unusual shape`,
			description: `You are not shaped like an ordinary human being. Describe how you are shaped and how this affects you when you select this Flaw. Whenever this causes you problems, you earn a point of Resolve.`
		},
		{
			id: `flaw34`,
			name: `vulnerability`,
			description: `You are vulnerable to one type of attack, effect, or weapon. Describe it in detail when you select this Flaw. Your active and passive defense ranks are halved against this attack, effect, or weapon. This Flaw is a Condition that grants you 1 extra point of Resolve at the start of every issue.`
		},
		{
			id: `flaw35`,
			name: `wanted`,
			description: `On a steel horse you ride. You are being actively hunted by authorities of some kind, so your hand is often forced in order to avoid being discovered, or to escape in those instances when you are. This Flaw is a Plot Hook that grants you 1 extra point of Resolve at the start of every issue`
		}
	],
	powers: [
		{ name: 'Adaptation', rank: null, range: null },
		{ name: 'Alternate Form', rank: null, range: null },
		{ name: 'Animal Control', rank: null, range: null },
		{ name: 'Animal Empathy', rank: null, range: null },
		{ name: 'Animal Mimicry', rank: null, range: null },
		{ name: 'Animation', rank: null, range: null },
		{ name: 'Armor', rank: null, range: null },
		{ name: 'Astral Projection', rank: null, range: null },
		{ name: 'Attuned', rank: null, range: null },
		{ name: 'Aura', rank: null, range: null },
		{ name: 'Banish', rank: null, range: null },
		{ name: 'Blast', rank: null, range: null },
		{ name: 'Blending', rank: null, range: null },
		{ name: 'Blind', rank: null, range: null },
		{ name: 'Blind Fighting', rank: null, range: null },
		{ name: 'Blink', rank: null, range: null },
		{ name: 'Boost', rank: null, range: null },
		{ name: 'Buff', rank: null, range: null },
		{ name: 'Clairvoyance', rank: null, range: null },
		{ name: 'Cloud Minds', rank: null, range: null },
		{ name: 'Communications', rank: null, range: null },
		{ name: 'Constructs', rank: null, range: null },
		{ name: 'Danger Sense', rank: null, range: null },
		{ name: 'Darkness', rank: null, range: null },
		{ name: 'Dazzle', rank: null, range: null },
		{ name: 'Deflection', rank: null, range: null },
		{ name: 'Density', rank: null, range: null },
		{ name: 'Detection', rank: null, range: null },
		{ name: 'Determination', rank: null, range: null },
		{ name: 'Dimensional Travel', rank: null, range: null },
		{ name: 'Dispel', rank: null, range: null },
		{ name: 'Drain', rank: null, range: null },
		{ name: 'Duplication', rank: null, range: null },
		{ name: 'Elemental Control', rank: null, range: null },
		{ name: 'Emotion Control', rank: null, range: null },
		{ name: 'Energy Absorption', rank: null, range: null },
		{ name: 'Ensnare', rank: null, range: null },
		{ name: 'Evasion', rank: null, range: null },
		{ name: 'Expertise', rank: null, range: null },
		{ name: 'Extra Limbs', rank: null, range: null },
		{ name: 'Flight', rank: null, range: null },
		{ name: 'Force Field', rank: null, range: null },
		{ name: 'Form', rank: null, range: null },
		{ name: 'Gestalt', rank: null, range: null },
		{ name: 'Growth', rank: null, range: null },
		{ name: 'Hard to Kill', rank: null, range: null },
		{ name: 'Healing', rank: null, range: null },
		{ name: 'Hibernation', rank: null, range: null },
		{ name: 'Hyper Breath', rank: null, range: null },
		{ name: 'Illusions', rank: null, range: null },
		{ name: 'Immortality', rank: null, range: null },
		{ name: 'Immunity', rank: null, range: null },
		{ name: 'Inanimate', rank: null, range: null },
		{ name: 'Invisibility', rank: null, range: null },
		{ name: 'Irritant', rank: null, range: null },
		{ name: 'Languages', rank: null, range: null },
		{ name: 'Leadership', rank: null, range: null },
		{ name: 'Leaping', rank: null, range: null },
		{ name: 'Life Drain', rank: null, range: null },
		{ name: 'Light/Effect', rank: null, range: null },
		{ name: 'Lightning Reflexes', rank: null, range: null },
		{ name: 'Luck', rank: null, range: null },
		{ name: 'Machine Control', rank: null, range: null },
		{ name: 'Martial Arts', rank: null, range: null },
		{ name: 'Master of Disguise', rank: null, range: null },
		{ name: 'Matter Chameleon', rank: null, range: null },
		{ name: 'Mind Blast', rank: null, range: null },
		{ name: 'Mind Control', rank: null, range: null },
		{ name: 'Nullify', rank: null, range: null },
		{ name: 'Omni-Power', rank: null, range: null },
		{ name: 'Phasing', rank: null, range: null },
		{ name: 'Plasticity', rank: null, range: null },
		{ name: 'Polymorph', rank: null, range: null },
		{ name: 'Portable Storehouse', rank: null, range: null },
		{ name: 'Possession', rank: null, range: null },
		{ name: 'Power Absorption', rank: null, range: null },
		{ name: 'Power Mimicry', rank: null, range: null },
		{ name: 'Precognition', rank: null, range: null },
		{ name: 'Preparation', rank: null, range: null },
		{ name: 'Psi-Screen', rank: null, range: null },
		{ name: 'Psychometry', rank: null, range: null },
		{ name: 'Quick Change', rank: null, range: null },
		{ name: 'Radar', rank: null, range: null },
		{ name: 'Regeneration', rank: null, range: null },
		{ name: 'Resistance', rank: null, range: null },
		{ name: 'Running', rank: null, range: null },
		{ name: 'Separation', rank: null, range: null }
	]
};
