export const CharacterForm = {
    abilities: ["Agility", "Intellect", "Might", "Perception", "Toughness", "Willpower"],
    talents: ["Academics", "Charm", "Command", "Covert", "Investigation", "Medicine", "Professional", "Science", "Streetwise", "Survival", "Technology", "Vehicles"]
}

