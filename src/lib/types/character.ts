export type Character = {
    heroName: string;
    realName: string;

    edge: number;
    health: number;
    resolve: number;
    heroPoints: number;

    abilities: Ability[];
    talents: Talent[];

    powers: Power[];
    perks: Perk[];
    flaws: Flaw[];
    gear: Gear[];

}


export type Ability = {
    id: string;
    name: string;
    rank: number;
    description: string;
    cost: number;
}

export type Talent = {
    id: string;
    name: string;
    rank: number;
    description: string;
    cost: number;
}

export type Power = {
    id: number;
    name: string;
    rank: number;
    description: string;
    cost: number;
    source: string;
    prosAndCons: ProOrCon[];
}

export type ProOrCon = {
    id: number;
    name: string;
    cost: number;
    rank: number;
    description: string;
}


export type Perk = {
    id: number;
    name: string;
    description: string;
    cost: number;
}

export type Flaw = {
    id: number;
    name: string;
    description: string;
}

export type Gear = {
    id: number;
    name: string;
    description: string;
}