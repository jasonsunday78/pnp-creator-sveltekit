import Localbase from 'localbase';
// https://github.com/samuk190/localbase

let db = new Localbase('db');

export async function getCharacters() {
    const characters = await db.collection('characters').get();
    return characters;
}

export async function addCharacter(character: any) {
    const characters = await db.collection('characters').add(character);
    return characters;
}

export async function updateCharacter(character: any) {
    const characters = await db.collection('characters').doc(character.id).update(character);
    return characters;
}

export async function deleteCharacter(character: any) {
    const characters = await db.collection('characters').doc(character.id).delete();
    return characters;
}